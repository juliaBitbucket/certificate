<?php
    $fileFolder = __DIR__ . '/tests/';
    $files = array_diff(scandir($fileFolder), array('..', '.'));
    if (empty($files)) {
        echo 'Файл с данным тестом не найден.';
        exit;
    }
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Список тестов</title>
        <style type="text/css">
            body {
                margin: 0px;
            }
            #menu {
                background: burlywood;
                height: 30px;
                padding: 20px 0px 0px 20px;
            }
            #menu a {
                text-decoration: none;
                margin-right: 10px;
            }
            #container {
                margin-left: 20px;
            }
        </style>
    </head>
    <body>
        <div id="menu">
            <a class="menu" href="admin.php">Загрузить файл</a>
            <a class="menu" href="list.php">Список тестов</a>
        </div>

        <div id="container">
            <h2>Выберите тест: </h2>
            <ul>
                <?php $i = 1; foreach ($files as $key => $fileName): ?>
                <li><a href="test.php?fileName=<?php echo $fileName; ?>">Тест <?php echo $i ?></a></li>
                <?php $i++; endforeach; ?>
            </ul>
        </div>
    </body>
</html>